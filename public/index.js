/* TODO Add run button and file name display left or top of editor and bottom panel min-height according to button space */
/* TODO Highlight button of selected SQL file */
/* TODO Have a maximum of one script running at a time to prevent errors */
/* TODO Move resize plugin to external file */
/* TODO Folder separation */
/* TODO Descriptions from first lines of comments */
/* TODO Improve console messages */
/* TODO Save button */
/* TODO Auto updating file list and other items */

let editor = ace.edit("editor");
let sqlFileList = [];

$(document).ready(function () {
  updateSqlFileList();
  initUI();
});

function updateSqlFileList() {
  $.post("/sql-list", function (data) {
    sqlFileList = data;
    $("#mainDiv").empty();
    data.forEach(function (sqlFile, index) {
      const element = $("<div class=\"SQLButton\" data-index=\"" + index + "\">" + sqlFile + "</div>");
      $("#mainDiv").append(element);
    });
    $(".SQLButton").click(function () {
      let index = $(this).attr('data-index');
      runSqlFile(index);
    });
  });
}

function runSqlFile(index) {
  $.post("/sql-get", {
      "file": sqlFileList[index]
    },
    function (data) {
      editor.setValue(data, -1);
    }
  );
  appConsole.clear();
  appConsole.append("Running...");
  $.post("/sql-run", {
      "file": sqlFileList[index]
    },
    updateConsole
  );
}

const appConsole = {
  append: function append(text, noNewLine) {
    let fullText = text + ((noNewLine) ? "" : "\n");
    $("#console").append(document.createTextNode(fullText));
  },
  appendHTML: function appendHTML(html) {
    $("#console").append(html);
  },
  clear: function clear() {
    $("#console").empty();
  }
}

function updateConsole(data) {
  appConsole.clear();
  if (data.error) {
    appConsole.append("Server has thrown an error:");
    appConsole.append(JSON.stringify(data.error, undefined, 2));
  }
  else {
    appConsole.append("Script run succesfully. Results:");
    appConsole.append(JSON.stringify(data.results, undefined, 2));
  }
}

function initEditor() {
  editor.setTheme("ace/theme/gruvbox");
  editor.session.setMode("ace/mode/mysql");
  editor.setFontSize("16px");
  editor.setOption("printMargin", false);
  editor.setReadOnly(false);
}

function initUI() {
  initEditor();
  $("#bottomPanelContainer").resizable({
    alsoResizeReverse: "#mainDivContainer",
    handles: "n",
    resize: function (event, ui) {
      editor.resize();
    }
  });
  $("#rightPanelContainer").resizable({
    alsoResizeReverse: "#editorContainer",
    handles: "w",
    resize: function (event, ui) {
      $("#editorContainer").css("height", "100%");
      editor.resize();
    }
  });
}

$.ui.plugin.add("resizable", "alsoResizeReverse", {

  start: function () {
    var that = $(this).resizable("instance"),
    o = that.options;

    $(o.alsoResizeReverse).each(function () {
      var el = $(this);
      el.data("ui-resizable-alsoresizeReverse", {
        width: parseFloat(el.width(), 10),
        height: parseFloat(el.height(), 10),
        left: parseFloat(el.css("left"), 10),
        top: parseFloat(el.css("top"), 10)
      });
    });
  },

  resize: function (event, ui) {
    var that = $(this).resizable("instance"),
    o = that.options,
    os = that.originalSize,
    op = that.originalPosition,
    delta = {
      height: (that.size.height - os.height) || 0,
      width: (that.size.width - os.width) || 0,
      top: (that.position.top - op.top) || 0,
      left: (that.position.left - op.left) || 0
    };

    $(o.alsoResizeReverse).each(function () {
      var el = $(this),
      start = $(this).data("ui-resizable-alsoresize-reverse"),
      style = {},
      css = el.parents(ui.originalElement[0]).length ?
        ["width", "height"] :
        ["width", "height", "top", "left"];

      $.each(css, function (i, prop) {
        var sum = (start[prop] || 0) - (delta[prop] || 0);
        if (sum && sum >= 0) {
          style[prop] = sum || null;
        }
      });

      el.css(style);
    });
  },

  stop: function () {
    $(this).removeData("resizable-alsoresize-reverse");
  }
});
