const express = require("express");
const app = express();
const fs = require("fs");
const path = require("path");
const mysql = require("mysql");
const port = 80;
var dbConfig = {
  host: "localhost",
  user: "root",
  password: "root"
};

app.use(express.static(__dirname + "/public"));

const sqlListPath = "/sql-list";
const sqlRunPath = "/sql-run";
const sqlGetPath = "/sql-get";
const sqlFilesDir = "sql";

app.all(sqlListPath, function (req, res) {
  console.log("Received request on " + sqlListPath + ".");
  fs.readdir(sqlFilesDir, function (err, files) {
    if (err) {
      console.log("Unable to read " + sqlFilesDir + " directory: " + err);
      return ;
    }
    res.json(files);
  });
});

app.all(sqlRunPath, express.urlencoded({extended: true}), function (req, res) {
  console.log("Received request on " + sqlRunPath + " containing " + req.body.file + ".");
  let sqlFile = path.resolve(sqlFilesDir, req.body.file);
  let fileContents = fs.readFileSync(sqlFile, "utf8");
  let connection = mysql.createConnection({
    multipleStatements: true,
    host     : "localhost",
    user     : "root",
    password : "root",
    insecureAuth : true
  });
  connection.query(fileContents, function (err, results, fields) {
    if (err) console.error(err);
    console.log(results);
    res.json({
      "error": err,
      "results": results
    });
  });
});

app.all(sqlGetPath, express.urlencoded({extended: true}), function (req, res) {
  console.log("Received request on " + sqlGetPath + " containing " + req.body.file + ".");
  let fileContents = fs.readFileSync(path.resolve(sqlFilesDir, req.body.file), "utf8");
  res.send(fileContents);
});

app.listen(port, () => console.log("Listening on port " + port + "."))
