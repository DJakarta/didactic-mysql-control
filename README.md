The application architecture and data flow is as follows:  

- A local SQL server is started
- The needed Node.js libraries are installed by running `install.bat`
- The Node.js server script `app.js` is started  

- When an user navigates to `http://localhost` the `app.js` gets the request and returns the front-end files from the
  folder `public`
- When the page loads, `index.js` is run and it requests the list of SQL files back from the server at
  `http://localhost/sql-list`
- The script `app.js` gets the request and returns the list of SQL files in the folder `sql`
- The `index.js` script receives the response and creates a button for each SQL file  

- When the user clicks a button, `index.js` gets the event and requests the contents of the SQL file from the server at
  `http://localhost/sql-get`
- The script `app.js` gets the request and returns the contents of the requested SQL file
- The `index.js` script receives the response and lists the file contents in the editor on the bottom left, after which
  it requests the server to run the SQL file at `http://localhost/sql-run`
- The script `app.js` gets the request, runs the file and returns the results of the run operation
- The `index.js` script receives the response and displays the results in the console in the bottom right