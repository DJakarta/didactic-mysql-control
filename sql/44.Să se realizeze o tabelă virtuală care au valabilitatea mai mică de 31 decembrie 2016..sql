USE `project`;
CREATE  OR REPLACE VIEW prodf_v AS
select id_produs COD_PRODUS,nume NUME_PRODUS,pret PRET,valabilitate VALABILITATE
from project.produs_farmacie 
where valabilitate<date('2019-12-31');
select * from prodf_v;
