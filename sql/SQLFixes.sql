/*
- Created "000. Drop database.sql" for deleting the database.
- Created "001. Initialize database.sql" for initializing the database and creating the tables.
- Used "IF NOT EXISTS" for table creation.
- Fixed table name from "pacienti" to "pacient" at creation.
- Changed "01. Să se redenumească tabela project.pacienti cu project.bolnav..sql" to select from "project.pacient" to 
  stop throwing errors for not finding table.
- Changed "08. Să se creeze tabela Pret_min pe baza tabelei project.produs_farmacie, care să conțină informații despre
  produsele cu prețul mai mic de 20 de lei..sql" to create "project.pret_minim" as in PDF.
- Changed "15.Să se șteargă din tabela Pret_min produsul cu id_produs=6..sql" and "16. Să se ștearga din tabela Pret_min
  produsul cu numele „ASPIRIN”..sql" to specify "project.pret_minim" database and table at deletion to stop throwing
  errors.
- Changed "16. Să se ștearga din tabela Pret_min produsul cu numele „ASPIRIN”..sql" to select from
  "project.produs_farmacie".
*/