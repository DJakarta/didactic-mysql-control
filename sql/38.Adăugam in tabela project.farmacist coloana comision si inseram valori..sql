SELECT * FROM project.farmacist;
alter table project.farmacist add comision numeric (2,2);
update project.farmacist set comision=0.2 where id_farmacist=1;
update project.farmacist set comision=0.1 where id_farmacist in (2,4,7,9);
update project.farmacist set comision=0.3 where id_farmacist in(3,5,6);
update project.farmacist set comision=0.05 where id_farmacist in(8,10);
