CREATE DATABASE IF NOT EXISTS project;

CREATE TABLE IF NOT EXISTS `project`.`producator` (
`id_producator` INT(11) NOT NULL,
`nume` VARCHAR(20) NOT NULL,
`adresa` VARCHAR(45) NOT NULL,
`telefon` VARCHAR(20) NOT NULL,
PRIMARY KEY (`id_producator`));

CREATE TABLE IF NOT EXISTS `project`.`compozitie` (
`id_compozitie` INT(11) NOT NULL,
`administrare` VARCHAR(45) NOT NULL,
`afectiuni` VARCHAR(45) NOT NULL,
PRIMARY KEY (`id_compozitie`));

CREATE TABLE IF NOT EXISTS `project`.`farmacist` (
`id_farmacist` INT(11) NOT NULL,
`nume` VARCHAR(45) NOT NULL,
`prenume` VARCHAR(45) NOT NULL,
`salariu` INT(20) NOT NULL,
PRIMARY KEY (`id_farmacist`));

CREATE TABLE IF NOT EXISTS `project`.`pacient` (
`id_pacient` INT(11) NOT NULL,
`nume` VARCHAR(20) NULL,
`prenume` VARCHAR(20) NULL,
`data_nastere` DATE NULL,
`telefon` VARCHAR(10) NULL,
PRIMARY KEY (`id_pacient`));

CREATE TABLE IF NOT EXISTS `project`.`produs_farmacie` (
`id_produs` INT(11) NOT NULL,
`nume` VARCHAR(45) NOT NULL,
`pret` INT(11) NOT NULL,
`valabilitate` DATE NOT NULL,
`id_compozitie` INT(11) NOT NULL,
`id_producator` INT(11) NOT NULL,
`stoc` INT(20) NOT NULL,
PRIMARY KEY (`id_produs`),
INDEX `id_producator_idx` (`id_producator` ASC) VISIBLE,
INDEX `id_compozitie_idx` (`id_compozitie` ASC) VISIBLE,
CONSTRAINT `id_producator`
FOREIGN KEY (`id_producator`)
REFERENCES `project`.`producator` (`id_producator`)
ON DELETE RESTRICT
ON UPDATE RESTRICT,
CONSTRAINT `id_compozitie`
FOREIGN KEY (`id_compozitie`)
REFERENCES `project`.`compozitie` (`id_compozitie`)
ON DELETE RESTRICT
ON UPDATE RESTRICT);

CREATE TABLE IF NOT EXISTS `project`.`reteta` (
`id_reteta` INT(2) NOT NULL,
`id_pacient` INT(11) NOT NULL,
`id_farmacist` INT(11) NULL,
`data_tiparire` DATE NOT NULL,
PRIMARY KEY (`id_reteta`),
INDEX `id_farmacist_idx` (`id_farmacist` ASC) VISIBLE,
INDEX `id_pacient_idx` (`id_pacient` ASC) VISIBLE,
CONSTRAINT `id_farmacist`
FOREIGN KEY (`id_farmacist`)
REFERENCES `project`.`farmacist` (`id_farmacist`)
ON DELETE RESTRICT
ON UPDATE RESTRICT,
CONSTRAINT `id_pacient`
FOREIGN KEY (`id_pacient`)
REFERENCES `project`.`pacient` (`id_pacient`)
ON DELETE RESTRICT
ON UPDATE RESTRICT);

CREATE TABLE IF NOT EXISTS `project`.`detalii_reteta` (
`id_reteta` INT(2) NOT NULL,
`id_produs` INT(2) NOT NULL,
`cantitate` INT(20) NOT NULL,
INDEX `id_produs_idx` (`id_produs` ASC) VISIBLE,
INDEX `id_reteta_idx` (`id_reteta` ASC) VISIBLE,
CONSTRAINT `id_produs`
FOREIGN KEY (`id_produs`)
REFERENCES `project`.`produs_farmacie` (`id_produs`)
ON DELETE RESTRICT
ON UPDATE RESTRICT,
CONSTRAINT `id_reteta`
FOREIGN KEY (`id_reteta`)
REFERENCES `project`.`reteta` (`id_reteta`)
ON DELETE RESTRICT
ON UPDATE RESTRICT);